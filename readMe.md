In order to run our summarizer run:
python summarizer.py -f [path to file]

if you need help run:
python summarizer.py -h 

This summarizer includes all of our weighting combined
in the folders there are several other variations that
focus on one or maybe two weights in order to summarize

In this summarizer there are the following weights:

- Tf-idf is the inspiration for the main baseline
- Extra cleaning on the data and other operations
- Weight added to sentences based off of their score density
- Weights added to the first and last 3 sentences of an article
- Weights added to the first and last sentence of each paragraph
- Weights added to sentences containing words that are in the title

In each folder there is a variation of the summarizer that includes
a readMe which describes which weights are being implemented in that
specific summarizer
