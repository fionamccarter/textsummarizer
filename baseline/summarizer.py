# this is the baseline summarizer



import argparse
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from string import punctuation
from nltk.probability import FreqDist
from heapq import nlargest
from collections import defaultdict
import sys

def parse_input():
    error_output='\n To run the summarizer run the following:\n\n'
    error_output+='\t\tpython summarizer.py -f [input file]\n'
    if len(sys.argv) < 2:# or len(sys.argv) > 3:
        print(error_output)
    args=sys.argv[1:]
    while(args):
        arg=args.pop(0)
        if arg == '-h':
            print(error_output)
        elif arg == '-f':
            arg=args.pop(0)
#            print('\nRunning summarizer on: '+arg+'\n')
            return arg
        else:
            print(error_output)
    exit()



def read(path_to_file):
    try:
        with open(path_to_file, 'r') as file:
            return file.read()
    except IOError as e:
            print("Error opening/reading file: {}".format(path_to_file))

def clean(input):
    translation = {
        ord('\f') : ' ',
        ord('\t') : ' ',
        ord('\n') : ' ',
        ord('\r') : None
    }
    return input.translate(translation)

def tokenize(input):
    stop_words = set(stopwords.words('english')+list(punctuation))
    words = word_tokenize(input.lower())

    return [
        sent_tokenize(input),
        [word for word in words if word not in stop_words]
    ]

def score_tokens(word_tokens, sentence_tokens):
    word_freq = FreqDist(word_tokens)
    ranking = defaultdict(int)

    for i, sentence in enumerate(sentence_tokens):
        for word in word_tokenize(sentence.lower()):
            if word in word_freq:
                ranking[i] += word_freq[word]

    return ranking

def summarize(ranks, sentences, length):
    if int(length) > len(sentences):
        print("Error, more sentences requested than available. Use --l (--length) flag to adjust.")
        exit()

    indexes = nlargest(length, ranks, key=ranks.get)
    final_sentences = [sentences[j] for j in sorted(indexes)]
    return ' '.join(final_sentences)


if __name__ == '__main__':
    file=parse_input()
    sentence_tokens, word_tokens=tokenize(clean(read(file)))
    sentence_ranks = score_tokens(word_tokens, sentence_tokens)
    print(summarize(sentence_ranks, sentence_tokens, 5))