import argparse

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from string import punctuation
from nltk.probability import FreqDist
from heapq import nlargest
from collections import defaultdict
import sys

AVG_DIFFS=0
WORD_COUNT=0

def parse_input():
    error_output='\n To run the summarizer run the following:\n\n'
    error_output+='\t\tpython summarizer.py -f [input file]\n'
    if len(sys.argv) < 2:# or len(sys.argv) > 3:
        print(error_output)
    args=sys.argv[1:]
    while(args):
        arg=args.pop(0)
        if arg == '-h':
            print(error_output)
        elif arg == '-f':
            arg=args.pop(0)
 #           print('\nRunning summarizer on: '+arg+'\n')
            return arg
        else:
            print(error_output)
    exit()

def read(path_to_file):
    try:
        with open(path_to_file, 'r') as file:
            return file.read()
    except IOError as e:
            print("Error opening/reading file: {}".format(path_to_file))

def split_into_paragraphs(input):
    result = []
    for paragraph in input.split('\n'):
        if not paragraph:
            continue
        else:
            result.append(paragraph)
    return result


def clean(input):
    translation = {
        ord('\f'): ' ',
        ord('\t'): ' ',
        ord('\n'): ' ',
        ord('\r'): None
    }
    result = []
    for p in input:
        result.append(p.translate(translation))
    return result


def tokenize(input):
    stop_words = set(stopwords.words('english')+list(punctuation))
    concat = ' '.join(input)
    words = word_tokenize(concat.lower())
    result = []
    global WORD_COUNT
    WORD_COUNT=len(words)
    for p in input:
        result.append(sent_tokenize(p))

##### HAD TO GET RID OF ' BECAUSE " 's " WAS BECOMING A FREQUENT WORD #####
    return [
        result,
        [word for word in words if word not in stop_words and '\'' not in word]
    ]


def score_tokens(word_tokens, sentence_tokens):
    word_freq = FreqDist(word_tokens)
#    ranking = defaultdict(int)
    results = []
    global WORD_COUNT
#    for word in word_freq:
#        print(word, word_freq[word])

    for p in sentence_tokens:
        ranking = defaultdict(int)
        for i, sentence in enumerate(p):
            for word in word_tokenize(sentence.lower()):
                if word in word_freq:
                    ranking[i] += word_freq[word]/WORD_COUNT
        results.append(ranking)
    return results


def summarize(ranks, sentences, length):
    if int(length) > len(sentences):
        print("Error, more sentences requested than available. Use --l (--length) flag to adjust.")
        exit()

    global AVG_DIFF

    all_indexes = nlargest(len(ranks), ranks, key=lambda i: ranks[i])
    #    print(all_indexes)
    diffs = 0
    for i in range(len(ranks) - 1):
        diffs += ranks[all_indexes[i]] - ranks[all_indexes[i + 1]]
    AVG_DIFF = diffs / (len(ranks) - 1)


    indexes = nlargest(length, ranks, key=lambda i: ranks[i])
#    print(indexes)
#    print(len(sentences))
    final_sentences = [sentences[j] for j in sorted(indexes)]
    return ' '.join(final_sentences)


if __name__ == '__main__':
    file = parse_input()
    input = clean(split_into_paragraphs(read(file)))
#    print(input)
    sentence_tokens, word_tokens=tokenize(input)#clean(split_into_paragraphs(read("./010.txt"))))
#    print(sentence_tokens)
#    print(word_tokens)
    sentence_ranks = score_tokens(word_tokens, sentence_tokens)
#    print(sentence_ranks)
#    print(len(sentence_ranks), len(sentence_ranks[0]))
    ranks=defaultdict(int)
    tokens=[]

########## add a little weight to the first and last sentence of each paragraph #####
    for i,p in enumerate(sentence_ranks):
        p[0]+=AVG_DIFFS/2
        p[len(p)-1]+=AVG_DIFFS/2
        for j,s in enumerate(p.values()):
            ranks[len(ranks)] = s
            tokens.append(sentence_tokens[i][j])
#    print(ranks)
    print(summarize(ranks, tokens, 5))


##########################################################
#### THIS FOR LOOP COMPUTES DENSITY OF FREQUENT WORDS ####
    for i,rank in enumerate(ranks.values()):
        ranks[i] = rank/len(tokens[i])
##########################################################
#    print(summarize(ranks, tokens, 5))
