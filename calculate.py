from rouge import Rouge
import os
import ast
Sdirectory = 'output/'
Hdirectory = 'Human/entertainment/'
rouge = Rouge()
total1=0
total2=0
totalL=0

count = 0
for filename in os.listdir(Sdirectory):
    for human in os.listdir(Hdirectory):
        if filename[-7:] == human[-7:]:
            filename=Sdirectory+filename
            human=" "+Hdirectory+human
            command = "rouge -f " + filename + human
            scores = os.popen(command).read()
            scores=ast.literal_eval(scores)
            total1 += scores[0]['rouge-1']['f']
            total2 += scores[0]['rouge-2']['f']
            totalL += scores[0]['rouge-l']['f']
            count+=1

print("Rouge-1: ", total1/count, "\nRouge-2: ", total2/count, "\nRouge-L: ", totalL/count)
